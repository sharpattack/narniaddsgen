import argparse
import datetime
import os
import sys
import typing


def defineType(out_file:typing.TextIO, base_name:str, class_name:str, fields:list[tuple[str, str]]):
    out_file.write(f"class {class_name}({base_name}):\n")
    out_file.write(f"    def __init__(self")
    for field in fields:
        out_file.write(f", {field[0]}: {field[1]}")
    out_file.write("):\n")
    for field in fields:
        out_file.write(f"        self.{field[0]} = {field[0]}\n")
    out_file.write(f"\n    def visit(self, visitor):\n")
    out_file.write(f"        return visitor.visit(self)\n")
    out_file.write(f"\n    def __str__(self):\n")
    out_file.write(f'        return f"')
    for index, field in enumerate(fields):
        out_file.write("{")
        out_file.write(f"self.{field[0]}")
        out_file.write("}")
        if index < len(field):
            out_file.write(" ")
    out_file.write('"\n')
    out_file.write(f"\n    def __repr__(self):\n")
    out_file.write(f"        return str(self)\n")


def defineAST(output_dir:str, base_name:str, types:dict[str, list[tuple[str, str]]]):
    source_file = os.path.join(output_dir, f"{base_name.lower()}.py")
    with open(source_file, 'w') as out_file:
        out_file.write(f"# Generated on {datetime.datetime.now()}\n")
        out_file.write("# DO NOT MODIFY\n\n")
        out_file.write("from NarniaDDSGen.Token import Token\n")
        if base_name != "Expr":
            out_file.write("from NarniaDDSGen.expr import Expr\n")
        out_file.write("\n\n")
        out_file.write(f"class {base_name}:\n")
        out_file.write(f"    def visit(self, visitor):\n")
        out_file.write(f"        return ''\n")
        out_file.write("\n\n")

        for index, key in enumerate(types.keys()):
            class_name = key
            fields = types[key]
            defineType(out_file, base_name, class_name, fields)
            out_file.write("\n")
            if index < len(types) - 1:
                out_file.write("\n")

def main() -> int:
    parser = argparse.ArgumentParser(description="Generate AST")
    parser.add_argument("output_directory", type=str)

    args = parser.parse_args()

    defineAST(args.output_directory, "Expr", {
        "Binary": [("left", "Expr"), ("operator", "Token"), ("right", "Expr")],
        "Grouping": [("expression", "Expr")],
        "Literal": [("value", "object")],
        "Unary": [("operator", "Token"), ("right", "Expr")],
        "Variable": [("name", "Token")],
    })

    defineAST(args.output_directory, "Stmt", {
        "Var": [("type", "list[Token]"), ("name", "Token"), ("size", "list[Expr]")],
        "ConstVar": [("type", "list[Token]"), ("name", "Token"), ("value", "Expr")],
        "Struct": [("name", "Token"), ("superStruct", "Expr"), ("statements", "list[Var]")],
        "Expression": [("expr", "Expr")],
        "Include": [("path", "str")],
    })
    return 0

if __name__ == "__main__":
    sys.exit(main())