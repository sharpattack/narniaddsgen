"""CLI interface for NarniaDDSGen project.
"""

import argparse
import os.path
import pathlib

from enum import Enum
from NarniaDDSGen.parser import Parser
from NarniaDDSGen.scanner import Scanner
from NarniaDDSGen.visitor import ASTVisitor


class ReturnCode(Enum):
    RETURN_OK = 0
    OUT_DIR_ERR = 1
    PARSE_ERR = 2
    SCAN_ERR = 3
    AST_ERR = 4


def main():  # pragma: no cover
    """
    The main function executes on commands:
    `python -m project_name` and `$ project_name `.
    """
    parser = argparse.ArgumentParser(
        prog='NarniaDDSGen',
        description='Generate .c/.h files from IDL files'
    )

    parser.add_argument('file', nargs='+', default=[], help='IDL files to be translated')
    parser.add_argument('-o', '--output', metavar='path', default='.', type=pathlib.Path,
                        help='Sets an output directory for generated files.')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s 0.1.0', help='Shows the current version')

    args = parser.parse_args()

    if os.path.exists(args.output) and not os.path.isdir(args.output):
        print(f"Error: '{args.output}' is a file")
        return ReturnCode.OUT_DIR_ERR.value

    if not os.path.exists(args.output):
        try:
            os.mkdir(args.output)
        except:
            print(f"Error: Couldn't create directory '{args.output}'")
            return ReturnCode.OUT_DIR_ERR.value

    for file in args.file:
        print(f"Processing the file {file}...")
        scanner = Scanner(file)

        tokens = scanner.scan_tokens()

        if scanner.had_error:
            return ReturnCode.SCAN_ERR.value

        scanner = Parser(file, tokens)
        ast = scanner.parse()

        if scanner.had_error:
            return ReturnCode.PARSE_ERR.value

        print("Generating Type definition files...")
        visitor = ASTVisitor(ast, file, args.output)
        visitor.write_ast()

        if visitor.has_error:
            return ReturnCode.AST_ERR.value

    return ReturnCode.RETURN_OK.value
