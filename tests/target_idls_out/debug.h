/*!
 * @file debug.h
 * This header file contains the declaration of the described types in the IDL file.
 *
 * This file was generated by the tool gen.
 */

#ifndef _debug_H_
#define _debug_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <stdbool.h>

/*!
 * @brief This struct represents the structure Debug defined by the user in the IDL file.
 * @ingroup debug
 */
typedef struct Debug
{
    char message[255];
} Debug;

struct ucdrBuffer;

bool Debug_serialize_topic(struct ucdrBuffer* writer, const Debug* topic);
bool Debug_deserialize_topic(struct ucdrBuffer* reader, Debug* topic);
uint32_t Debug_size_of_topic(const Debug* topic, uint32_t size);


#ifdef __cplusplus
}
#endif

#endif // _debug_H_