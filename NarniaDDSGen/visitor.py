import os
import re
import weakref
from typing import Union

from NarniaDDSGen.stmt import *
from NarniaDDSGen.expr import *
from NarniaDDSGen.token_type import TokenType


def slice_firsts_at_most_tokens(lst: list, n):
    max_n = min(len(lst), n)
    res = []
    for i in range(max_n):
        if isinstance(lst[i], Token):
            res.append(lst[i].type)
    return res


class VarType:
    def __init__(self):
        self.inner: Union[VarType, None] = None
        self.parent: Union[VarType, None] = None
        self.array_size: Union[str, None] = None
        self.tokens: list[Token] = []
        self.literal_name: Union[str, None] = None
        self.is_dynamic_size: bool = False

    def ctype(self):
        if self.inner:
            return self.inner.ctype()
        elif self.literal_name:
            return self.literal_name
        simple_type_dict = {
            (TokenType.BOOLEAN,): "bool",
            (TokenType.CHAR,): "char",
            (TokenType.OCTET,): "uint8_t",
            (TokenType.SHORT,): "int16_t",
            (TokenType.UNSIGNED, TokenType.SHORT): "uint16_t",
            (TokenType.LONG,): "int32_t",
            (TokenType.UNSIGNED, TokenType.LONG): "uint32_t",
            (TokenType.LONG, TokenType.LONG): "int64_t",
            (TokenType.UNSIGNED, TokenType.LONG, TokenType.LONG): "uint64_t",
            (TokenType.FLOAT,): "float",
            (TokenType.DOUBLE,): "double",
            (TokenType.STRING,): "char",
        }

        return simple_type_dict[tuple(self.tokens)]

    def ctypesize(self) -> int:
        simple_type_dict = {
            (TokenType.BOOLEAN,): 1,
            (TokenType.CHAR,): 1,
            (TokenType.OCTET,): 1,
            (TokenType.UNSIGNED, TokenType.OCTET,): 2,
            (TokenType.SHORT,): 2,
            (TokenType.UNSIGNED, TokenType.SHORT): 2,
            (TokenType.LONG,): 4,
            (TokenType.UNSIGNED, TokenType.LONG): 4,
            (TokenType.LONG, TokenType.LONG): 8,
            (TokenType.UNSIGNED, TokenType.LONG, TokenType.LONG): 8,
            (TokenType.FLOAT,): 4,
            (TokenType.DOUBLE,): 8,
        }

        return simple_type_dict[tuple(self.tokens)]

    def count_dynamic_arrays(self) -> int:
        count = 0
        tmp = self
        while tmp:
            if tmp.is_dynamic_size:
                count += 1
            tmp = tmp.inner
        return count

    def has_array_parent(self) -> bool:
        return bool(self.parent) and bool(self.parent.array_size)

    def get_formated_arrays_size(self):
        formated_size = ""
        if self.inner:
            tmp = self
            while tmp:
                if not tmp.inner:
                    break
                formated_size = formated_size + f"[{tmp.array_size}]"
                tmp = tmp.inner
        return formated_size

    def get_leaf(self):
        tmp = self
        while tmp.inner:
            tmp = tmp.inner
        return tmp

    def __str__(self):
        desc = ""

        if self.inner:
            tmp = self
            while tmp:
                if not tmp.inner:
                    desc = tmp.ctype() + desc
                    break
                desc = desc + f"[{tmp.array_size}]"
                tmp = tmp.inner
        else:
            desc = self.ctype()

        return desc

    def __repr__(self):
        return str(self)


def token_list_to_type_str(tokens: list[Union[Literal, Token]]) -> (str, list[str]):
    type_str = ""
    simple_type_dict = {
        (TokenType.BOOLEAN,): "bool",
        (TokenType.CHAR,): "int8_t",
        (TokenType.OCTET,): "uint8_t",
        (TokenType.SHORT,): "int16_t",
        (TokenType.UNSIGNED, TokenType.SHORT): "uint16_t",
        (TokenType.LONG,): "int32_t",
        (TokenType.UNSIGNED, TokenType.LONG): "uint32_t",
        (TokenType.LONG, TokenType.LONG): "int64_t",
        (TokenType.UNSIGNED, TokenType.LONG, TokenType.LONG): "uint64_t",
        (TokenType.FLOAT,): "float",
        (TokenType.DOUBLE,): "double",
        (TokenType.STRING,): "char",
    }

    sizes = []
    index = 0
    nb_sequence = 0
    while tokens[index].type == TokenType.SEQUENCE:
        index += 1
        nb_sequence += 1

    simple_type_slice = tuple(slice_firsts_at_most_tokens(tokens[index:], 3))
    found = False
    while len(simple_type_slice) and not found:
        if simple_type_slice in simple_type_dict.keys():
            type_str += f"{simple_type_dict[simple_type_slice]}"
            found = True
            index += len(simple_type_slice)
        elif len(simple_type_slice) > 1:
            simple_type_slice = tuple(simple_type_slice[-1:])
        elif len(simple_type_slice) == 1 and simple_type_slice[0] == TokenType.IDENTIFIER:
            type_str += f"{tokens[index:][0].lexeme}"
            found = True
            index += 1
        else:
            break

    if not found:
        raise Exception(slice_firsts_at_most_tokens(tokens[index:], 3))

    for token in tokens[index:]:
        sizes.append(str(token.value))
    sizes.reverse()

    return type_str, sizes


class ASTVisitor:
    def __init__(self, AST: list[Stmt], out_file: os.path, out_directory: os.path):
        self.list_ast = AST
        self.has_error = False
        self.basename = self.idl_module_from_path(out_file)
        self.module_name = to_snake_case(self.basename)
        self.source_file = None
        self.header_file = None
        self.source_file_path = self.idl_filename_to_source(out_file, out_directory)
        self.header_file_path = self.idl_filename_to_header(out_file, out_directory)
        self.includes = []
        self._finalizer = weakref.finalize(self, self._cleanup_files, [self.source_file, self.header_file])

    @staticmethod
    def _cleanup_files(files: list):
        for file in files:
            if file:
                file.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._finalizer()

    def token_list_to_VarType_inner(self, tokens: list[Union[Literal, Token]], var_sizes: list[Expr],
                                    parent: Union[None, VarType]) -> [VarType,
                                                                      None]:
        if not tokens and not var_sizes:
            return None

        var = VarType()
        var.parent = parent
        if var_sizes:
            var.array_size = self.visit(var_sizes[0])
            del var_sizes[0]
            var.inner = self.token_list_to_VarType_inner(tokens, var_sizes, var)
        else:
            token = tokens[0]
            if token.type == TokenType.IDENTIFIER:
                var.literal_name = token.lexeme
                var.tokens = [token]
                del tokens[0]
            elif token.type == TokenType.SEQUENCE:
                del tokens[0]
                var.is_dynamic_size = True
                var.tokens = [token]
                var.inner = self.token_list_to_VarType_inner(tokens, var_sizes, var)
                var.array_size = self.visit(tokens[0])
                del tokens[0]
            elif token.type == TokenType.STRING:
                del tokens[0]
                var.tokens = [token]
                var_inner = VarType()
                var_inner.tokens = [TokenType.CHAR]
                var_inner.parent = var
                var.inner = var_inner
                var.array_size = self.visit(tokens[0])
                del tokens[0]
            else:
                firsts_token = slice_firsts_at_most_tokens(tokens, 3)
                var.tokens = firsts_token[:]
                for i in range(len(firsts_token)):
                    del tokens[0]

        return var

    def token_list_to_VarType(self, tokens: list[Union[Literal, Token]], var_sizes: list[Expr]) -> VarType:
        if not var_sizes:
            var_sizes = []
        return self.token_list_to_VarType_inner(tokens[:], var_sizes[:], None)

    def idl_module_from_path(self, idl_filename: os.path):
        _, filename = os.path.split(idl_filename)
        filename_no_ext, _ = os.path.splitext(filename)
        return filename_no_ext

    def idl_target_path(self, idl_filename: os.path, target_path: os.path):
        return os.path.join(target_path, self.module_name)

    def idl_filename_to_source(self, idl_filename: os.path, target_path: os.path):
        basename = self.idl_target_path(idl_filename, target_path)
        return f"{basename}.c"

    def idl_filename_to_header(self, idl_filename: os.path, target_path: os.path):
        basename = self.idl_target_path(idl_filename, target_path)
        return f"{basename}.h"

    def write_ast(self):
        if not self.source_file:
            self.source_file = open(self.source_file_path, 'w')
        if not self.header_file:
            self.header_file = open(self.header_file_path, 'w')

        # On fait une premiere visite pour recupere la liste des includes
        for node in self.list_ast:
            if isinstance(node, Include):
                self.visit_Include(node)

        self.init_header()
        self.init_source()
        for node in self.list_ast:
            node.visit(self)

        self.end_header()
        self.end_source()

        if self.source_file and not self.source_file.closed:
            self.source_file.close()
        if self.header_file and not self.header_file.closed:
            self.header_file.close()

    def visit(self, node):
        if isinstance(node, Var):
            return self.visit_Var(node)
        elif isinstance(node, ConstVar):
            return self.visit_ConstVar(node)
        elif isinstance(node, Struct):
            return self.visit_Struct(node)
        elif isinstance(node, Include):
            return None
        elif isinstance(node, Expression):
            return self.visit_Expression(node)
        elif isinstance(node, Binary):
            return self.visit_Binary(node)
        elif isinstance(node, Grouping):
            return self.visit_Grouping(node)
        elif isinstance(node, Literal):
            return self.visit_Literal(node)
        elif isinstance(node, Unary):
            return self.visit_Unary(node)
        elif isinstance(node, Variable):
            return self.visit_Variable(node)
        else:
            self.has_error = True
            print(f"Invalide type '{type(node).__name__}'")
            return

    def visit_Var(self, var: Var):
        print("Unimplemented 'visit_Var'")
        self.has_error = True

    def visit_ConstVar(self, var: ConstVar):
        var_name, size_lst = token_list_to_type_str(var.type)
        if size_lst:
            print("Unimplemented const array")
            self.has_error = True
            return

        suffix = ""
        if var_name == "float":
            suffix = 'f' if '.' in var.value.visit(self) else '.0f'
        elif var_name not in ["double", "bool"]:
            suffix = f"{'U' if 'u' in var_name else ''}{'L' if '64' in var_name else ''}L"

        self.header_file.write(f"#define {var.name.lexeme} {var.value.visit(self)}{suffix}\n\n")

    def visit_Struct_header(self, var: Struct):
        # Doxygen documentation
        self.header_file.write("/*!\n")
        self.header_file.write(
            f" * @brief This struct represents the structure {var.name.lexeme} defined by the user in the IDL file.\n")
        self.header_file.write(f" * @ingroup {self.basename}\n")
        self.header_file.write(" */\n")
        # Declaring struct
        self.header_file.write(f"typedef struct {var.name.lexeme}\n")
        self.header_file.write("{\n")
        # Iter over all attributes
        for variable in var.statements:
            var_data = self.token_list_to_VarType(variable.type, variable.size)

            nb_dynamic_arrays = var_data.count_dynamic_arrays()
            if nb_dynamic_arrays == 1:
                self.header_file.write(f"    uint32_t {variable.name.lexeme}_size")
                tmp = var_data
                while not tmp.is_dynamic_size:
                    tmp = var_data.inner
                if tmp.parent and tmp.parent.array_size:
                    self.header_file.write(f"[{tmp.parent.array_size}]")
                self.header_file.write(f";\n")
            else:
                for i in range(nb_dynamic_arrays):
                    self.header_file.write(f"    uint32_t {variable.name.lexeme}_size{i}")
                    nb_dynamic_arrays_found = 0
                    tmp = var_data

                    while nb_dynamic_arrays_found < (i+1):
                        if tmp.is_dynamic_size:
                            nb_dynamic_arrays_found += 1
                        if nb_dynamic_arrays_found < (i + 1):
                            tmp = tmp.inner
                    if tmp.parent and tmp.parent.array_size:
                        self.header_file.write(f"[{tmp.parent.array_size}]")
                    self.header_file.write(f";\n")

            self.header_file.write(
                f"    {var_data.ctype()} {variable.name.lexeme}{var_data.get_formated_arrays_size()};\n")

        self.header_file.write(f"}} {var.name.lexeme};\n")
        self.header_file.write("\n")
        self.header_file.write("struct ucdrBuffer;\n")
        self.header_file.write("\n")
        self.header_file.write(
            f"bool {var.name.lexeme}_serialize_topic(struct ucdrBuffer* writer, const {var.name.lexeme}* topic);\n")
        self.header_file.write(
            f"bool {var.name.lexeme}_deserialize_topic(struct ucdrBuffer* reader, {var.name.lexeme}* topic);\n")
        self.header_file.write(
            f"uint32_t {var.name.lexeme}_size_of_topic(const {var.name.lexeme}* topic, uint32_t size);\n")
        self.header_file.write("\n")

    def visit_Struct_source(self, var: Struct):
        serialization_lines = []
        deserialization_lines = []
        size_lines = []

        for variable in var.statements:
            var_data = self.token_list_to_VarType(variable.type, variable.size)

            if not var_data.inner:
                # Pas de type interne, on n'est que sur des cas simples
                if var_data.literal_name:
                    # type custom
                    serialization_lines.append(
                        f"        success &= {var_data.literal_name}_serialize_topic(writer, &topic->{variable.name.lexeme});\n")
                    deserialization_lines.append(
                        f"        success &= {var_data.literal_name}_deserialize_topic(reader, &topic->{variable.name.lexeme});\n")
                    size_lines.append(
                        f"        size += {var_data.literal_name}_size_of_topic(&topic->{variable.name.lexeme}, size);\n")
                else:
                    # type standard
                    serialization_lines.append(
                        f"        success &= ucdr_serialize_{var_data.ctype()}(writer, topic->{variable.name.lexeme});\n")
                    deserialization_lines.append(
                        f"        success &= ucdr_deserialize_{var_data.ctype()}(reader, &topic->{variable.name.lexeme});\n")
                    size_lines.append(
                        f"        size += ucdr_alignment(size, {var_data.ctypesize()}) + {var_data.ctypesize()};\n")

            else:
                # Ici, on est forcement dans des tableaux
                nb_dynamic_arrays = var_data.count_dynamic_arrays()
                count_dynamic_arrays = 0
                ident_level = {
                    's': 1,
                    'd': 1,
                    'z': 1,
                }
                tmp = var_data
                array_sizes = []
                while tmp.inner:
                    # dernier niveau
                    if not tmp.inner.inner:
                        if tmp.inner.literal_name:
                            # type custom
                            # On serialize chaque element a la main

                            array_sizes.append(f"i{'' if not len(array_sizes) else len(array_sizes) + 1}")
                            # on est peut etre dans un tableau dynamic
                            is_dynamic = tmp.is_dynamic_size
                            if is_dynamic:
                                var_size_name = f"{variable.name.lexeme}_size{count_dynamic_arrays if nb_dynamic_arrays > 1 else ''}"
                                serialization_lines.append(
                                    f"    {'    ' * ident_level['s']}success &= ucdr_serialize_uint32_t("
                                    f"writer, topic->{var_size_name});\n")
                                serialization_lines.append(
                                    f"    {'    ' * ident_level['s']}for(size_t {array_sizes[-1]} = 0; {array_sizes[-1]} < topic->{var_size_name}; ++{array_sizes[-1]})\n")

                                deserialization_lines.append(
                                    f"    {'    ' * ident_level['d']}success &= ucdr_deserialize_uint32_t("
                                    f"reader, &topic->{var_size_name});\n")
                                deserialization_lines.append(
                                    f"    {'    ' * ident_level['d']}if(topic->{var_size_name} > {tmp.array_size})\n")
                                deserialization_lines.append(f"    {'    ' * ident_level['d']}{{\n")
                                deserialization_lines.append(
                                    f"    {'    ' * (ident_level['d'] + 1)}reader->error = true;\n")
                                deserialization_lines.append(f"    {'    ' * ident_level['d']}}}\n")
                                deserialization_lines.append(f"    {'    ' * ident_level['d']}else\n")
                                deserialization_lines.append(f"    {'    ' * ident_level['d']}{{\n")
                                ident_level['d'] += 1
                                deserialization_lines.append(
                                    f"    {'    ' * ident_level['d']}for(size_t {array_sizes[-1]} = 0; {array_sizes[-1]} < topic->{var_size_name}; ++{array_sizes[-1]})\n")

                                size_lines.append(f"    {'    ' * ident_level['z']}size += ucdr_alignment(size, 4) + 4;\n")
                                size_lines.append(
                                    f"    {'    ' * ident_level['z']}for(size_t {array_sizes[-1]} = 0; {array_sizes[-1]} < topic->{var_size_name}; ++{array_sizes[-1]})\n")
                            else:
                                serialization_lines.append(f"    {'    ' * ident_level['s']}for(size_t {array_sizes[-1]} = 0; {array_sizes[-1]} < sizeof(topic->{variable.name.lexeme}")
                                deserialization_lines.append(f"    {'    ' * ident_level['d']}for(size_t {array_sizes[-1]} = 0; {array_sizes[-1]} < sizeof(topic->{variable.name.lexeme}")
                                size_lines.append(f"    {'    ' * ident_level['z']}for(size_t {array_sizes[-1]} = 0; {array_sizes[-1]} < sizeof(topic->{variable.name.lexeme}")
                                for size_index in array_sizes[:-1]:
                                    serialization_lines.append(f"[{size_index}]")
                                    deserialization_lines.append(f"[{size_index}]")
                                    size_lines.append(f"[{size_index}]")
                                for_static_cont = f") / sizeof({tmp.inner.literal_name}); ++{array_sizes[-1]})\n"
                                serialization_lines.append(for_static_cont)
                                deserialization_lines.append(for_static_cont)
                                size_lines.append(for_static_cont)
                            serialization_lines.append(f"    {'    ' * ident_level['s']}{{\n")
                            deserialization_lines.append(f"    {'    ' * ident_level['d']}{{\n")
                            size_lines.append(f"    {'    ' * ident_level['z']}{{\n")
                            ident_level['s'] += 1
                            ident_level['d'] += 1
                            ident_level['z'] += 1
                            serialization_lines.append(
                                f"    {'    ' * ident_level['s']}success &= {tmp.inner.literal_name}_serialize_topic(writer, &topic->{variable.name.lexeme}")
                            deserialization_lines.append(
                                f"    {'    ' * ident_level['d']}success &= {tmp.inner.literal_name}_deserialize_topic(reader, &topic->{variable.name.lexeme}")
                            size_lines.append(
                                f"    {'    ' * ident_level['z']}size += {tmp.inner.literal_name}_size_of_topic(&topic->{variable.name.lexeme}")
                            for size_index in array_sizes[:-1]:
                                serialization_lines.append(f"[{size_index}]")
                                deserialization_lines.append(f"[{size_index}]")
                                size_lines.append(f"[{size_index}]")
                            serialization_lines.append(f"[{array_sizes[-1]}]);\n")
                            deserialization_lines.append(f"[{array_sizes[-1]}]);\n")
                            size_lines.append(f"[{array_sizes[-1]}], size);\n")
                            ident_level['s'] -= 1
                            ident_level['d'] -= 1
                            ident_level['z'] -= 1
                            serialization_lines.append(f"    {'    ' * ident_level['s']}}}\n")
                            deserialization_lines.append(f"    {'    ' * ident_level['d']}}}\n")
                            size_lines.append(f"    {'    ' * ident_level['z']}}}\n")
                        elif tmp.tokens and tmp.tokens[0].type == TokenType.STRING:
                            # String
                            # on a directement une fonction special pour serialiser ca
                            array_index = ''.join([f'[{i}]' for i in array_sizes])
                            serialization_lines.append(f"    {'    ' * ident_level['s']}success &= ucdr_serialize_string("
                                                       f"writer, topic->{variable.name.lexeme}")
                            deserialization_lines.append(f"    {'    ' * ident_level['d']}success &= ucdr_deserialize_string("
                                                       f"reader, topic->{variable.name.lexeme}{array_index}")
                            size_lines.append(f"    {'    ' * ident_level['z']}size += ucdr_alignment(size, 4) + 4 + ("
                                              f"uint32_t)strlen(topic->{variable.name.lexeme}")
                            for size_index in array_sizes[:]:
                                tab = f"[{size_index}]"
                                serialization_lines.append(tab)
                                size_lines.append(tab)
                            serialization_lines.append(f");\n")
                            deserialization_lines.append(f", {tmp.array_size});\n")
                            size_lines.append(f") + 1;\n")
                        else:
                            # Type de base
                            # On a des fonction generique par type
                            if tmp.is_dynamic_size:
                                var_size_name = f"{variable.name.lexeme}_size{count_dynamic_arrays if nb_dynamic_arrays > 1 else ''}"
                                all_array_indexes = "".join([f"[{array_size}]" for array_size in array_sizes])
                                current_array_index_size = "" if not array_sizes else f"[{array_sizes[-1]}]"

                                serialization_lines.append(f"    {'    ' * ident_level['s']}success &= ucdr_serialize_uint32_t("
                                                           f"writer, topic->{var_size_name}[{array_sizes[-1]}]);\n")
                                serialization_lines.append(
                                    f"    {'    ' * ident_level['s']}success &= ucdr_serialize_array_{tmp.inner.ctype()}(writer, topic->{variable.name.lexeme}{all_array_indexes}, topic->{var_size_name}[{array_sizes[-1]}]);\n")

                                deserialization_lines.append(f"    {'    ' * ident_level['d']}success &= ucdr_deserialize_uint32_t(reader, &topic->{var_size_name}{current_array_index_size});\n")
                                deserialization_lines.append(f"    {'    ' * ident_level['d']}if(topic->{var_size_name}{current_array_index_size} > {tmp.array_size})\n")
                                deserialization_lines.append(f"    {'    ' * ident_level['d']}{{\n")
                                deserialization_lines.append(f"    {'    ' * (ident_level['d'] + 1)}reader->error = true;\n")
                                deserialization_lines.append(f"    {'    ' * ident_level['d']}}}\n")
                                deserialization_lines.append(f"    {'    ' * ident_level['d']}else\n")
                                deserialization_lines.append(f"    {'    ' * ident_level['d']}{{\n")
                                deserialization_lines.append(f"    {'    ' * (ident_level['d'] + 1)}success &= ucdr_deserialize_array_{tmp.inner.ctype()}(reader, topic->{variable.name.lexeme}{all_array_indexes}, topic->{var_size_name}{current_array_index_size});\n")
                                deserialization_lines.append(f"    {'    ' * ident_level['d']}}}\n")

                                size_lines.append(f"    {'    ' * ident_level['z']}size += ucdr_alignment(size, 4) + 4);\n")
                                size_lines.append(f"    {'    ' * ident_level['z']}size += ucdr_alignment(size, {tmp.inner.ctypesize()}) + ({tmp.inner.ctypesize()} * topic->{var_size_name}[{array_sizes[-1]}]);\n")
                            else:
                                serialization_lines.append(
                                    f"    {'    ' * ident_level['s']}success &= ucdr_serialize_array_{tmp.inner.ctype()}(writer, topic->{variable.name.lexeme}, sizeof(topic->{variable.name.lexeme}) / sizeof({tmp.inner.ctype()}));\n")
                                deserialization_lines.append(f"    {'    ' * ident_level['d']}success &= ucdr_deserialize_array_{tmp.inner.ctype()}(reader, topic->{variable.name.lexeme}, sizeof(topic->{variable.name.lexeme}) / sizeof({tmp.inner.ctype()}));\n")
                                size_lines.append(f"    {'    ' * ident_level['z']}size += ucdr_alignment(size, {tmp.inner.ctypesize()}) + sizeof(topic->{variable.name.lexeme});\n")

                        # On refermes les accolades
                        for k in ident_level.keys():
                            for i in range(ident_level[k]-1, 0, -1):
                                closing_curly_brace = f"    {'    ' * i}}}\n"
                                if k == 's':
                                    serialization_lines.append(closing_curly_brace)
                                elif k == 'd':
                                    deserialization_lines.append(closing_curly_brace)
                                elif k == 'z':
                                    size_lines.append(closing_curly_brace)
                    elif tmp.is_dynamic_size:
                        # tableau dynamique
                        var_size_name = f"{variable.name.lexeme}_size{count_dynamic_arrays if nb_dynamic_arrays > 1 else ''}"
                        current_array_index_size = "" if not array_sizes else f"[{array_sizes[-1]}]"

                        array_sizes.append(f"i{'' if not len(array_sizes) else len(array_sizes) + 1}")

                        # serialization
                        serialization_lines.append(f"    {'    ' * ident_level['s']}success &= ucdr_serialize_uint32_t("
                                                   f"writer, topic->{var_size_name}{current_array_index_size});\n")

                        serialization_lines.append(
                            f"    {'    ' * ident_level['s']}for(size_t {array_sizes[-1]} = 0; {array_sizes[-1]} < topic->{var_size_name}{current_array_index_size}; ++{array_sizes[-1]})\n")
                        serialization_lines.append(f"    {'    ' * ident_level['s']}{{\n")

                        # deserialization
                        deserialization_lines.append(f"    {'    ' * ident_level['d']}success &= ucdr_deserialize_uint32_t(reader, &topic->{var_size_name}{current_array_index_size});\n")
                        deserialization_lines.append(f"    {'    ' * ident_level['d']}if(topic->{var_size_name}{current_array_index_size} > {tmp.array_size})\n")
                        deserialization_lines.append(f"    {'    ' * ident_level['d']}{{\n")
                        deserialization_lines.append(f"    {'    ' * (ident_level['d'] + 1)}reader->error = true;\n")
                        deserialization_lines.append(f"    {'    ' * ident_level['d']}}}\n")
                        deserialization_lines.append(f"    {'    ' * ident_level['d']}else\n")
                        deserialization_lines.append(f"    {'    ' * ident_level['d']}{{\n")
                        ident_level['d'] += 1
                        deserialization_lines.append(f"    {'    ' * ident_level['d']}for(size_t {array_sizes[-1]} = 0; {array_sizes[-1]} < topic->{var_size_name}{current_array_index_size}; ++{array_sizes[-1]})\n")
                        deserialization_lines.append(f"    {'    ' * ident_level['d']}{{\n")

                        # taille
                        size_lines.append(f"    {'    ' * ident_level['z']}size += ucdr_alignment(size, 4) + 4;\n")
                        size_lines.append(
                            f"    {'    ' * ident_level['z']}for(size_t {array_sizes[-1]} = 0; {array_sizes[-1]} < topic->{var_size_name}{current_array_index_size}; ++{array_sizes[-1]})\n")
                        size_lines.append(f"    {'    ' * ident_level['z']}{{\n")

                        ident_level['s'] += 1
                        ident_level['d'] += 1
                        ident_level['z'] += 1
                        count_dynamic_arrays += 1
                    else:
                        # tableau static
                        before_size = ''.join([f'[{s}]' for s in array_sizes])
                        array_sizes.append(f"i{'' if not len(array_sizes) else len(array_sizes) + 1}")
                        serialization_lines.append(
                            f"    {'    ' * ident_level['s']}for(size_t {array_sizes[-1]} = 0; {array_sizes[-1]} < sizeof("
                            f"topic->{variable.name.lexeme}{before_size}) / sizeof(topic->{variable.name.lexeme}"
                            f"{before_size}[0]); ++{array_sizes[-1]})\n")
                        serialization_lines.append(f"    {'    ' * ident_level['s']}{{\n")

                        deserialization_lines.append(
                            f"    {'    ' * ident_level['d']}for(size_t {array_sizes[-1]} = 0; {array_sizes[-1]} < sizeof("
                            f"topic->{variable.name.lexeme}{before_size}) / sizeof(topic->{variable.name.lexeme}"
                            f"{before_size}[0]); ++{array_sizes[-1]})\n")
                        deserialization_lines.append(f"    {'    ' * ident_level['d']}{{\n")

                        size_lines.append(
                            f"    {'    ' * ident_level['z']}for(size_t {array_sizes[-1]} = 0; {array_sizes[-1]} < sizeof("
                            f"topic->{variable.name.lexeme}{before_size}) / sizeof(topic->{variable.name.lexeme}"
                            f"{before_size}[0]); ++{array_sizes[-1]})\n")
                        size_lines.append(f"    {'    ' * ident_level['z']}{{\n")

                        ident_level['s'] += 1
                        ident_level['d'] += 1
                        ident_level['z'] += 1

                    tmp = tmp.inner

        # serialisation
        self.source_file.write(
            f"\nbool {var.name.lexeme}_serialize_topic(ucdrBuffer* writer, const {var.name.lexeme}* topic)\n")
        self.source_file.write(f"{{\n")
        self.source_file.write(f"    bool success = true;\n")
        for line in serialization_lines:
            self.source_file.write(line)
        self.source_file.write(f"    return success && !writer->error;\n")
        self.source_file.write(f"}}\n\n")

        # deserialisation
        self.source_file.write(
            f"bool {var.name.lexeme}_deserialize_topic(ucdrBuffer* reader, {var.name.lexeme}* topic)\n")
        self.source_file.write(f"{{\n")
        self.source_file.write(f"    bool success = true;\n")
        for line in deserialization_lines:
            self.source_file.write(line)
        self.source_file.write(f"    return success && !reader->error;\n")
        self.source_file.write(f"}}\n\n")

        # size of topic
        self.source_file.write(
            f"uint32_t {var.name.lexeme}_size_of_topic(const {var.name.lexeme}* topic, uint32_t size)\n")
        self.source_file.write(f"{{\n")
        self.source_file.write(f"    uint32_t previousSize = size;\n")
        for line in size_lines:
            self.source_file.write(line)
        self.source_file.write(f"    return size - previousSize;\n")
        self.source_file.write(f"}}\n")

    def visit_Struct(self, var: Struct):
        self.visit_Struct_header(var)
        self.visit_Struct_source(var)

    def visit_Include(self, var: Include):
        # Le scanner garantis la presence des guillemets
        striped_path = var.path[1:-1]
        basename = self.idl_module_from_path(striped_path)
        path, _ = os.path.split(striped_path)
        self.includes.append(os.path.join(path, f"{to_snake_case(basename)}.h"))


    def visit_Expression(self, var: Expression):
        return var.expr.visit(self)

    def visit_Binary(self, var: Binary):
        return f"{var.left.visit(self)} {var.operator.lexeme} {var.right.visit(self)}"

    def visit_Grouping(self, var: Grouping):
        return var.expression.visit(self)

    def visit_Literal(self, var: Literal):
        return f"{var.value}"

    def visit_Unary(self, var: Unary):
        return f"{var.operator.lexeme} {var.right.visit(self)}"

    def visit_Variable(self, var: Variable):
        return f"{var.name.lexeme}"

    def init_header(self):
        self.header_file.write(f"/*!\n")
        self.header_file.write(f" * @file {self.basename}.h\n")
        self.header_file.write(
            f" * This header file contains the declaration of the described types in the IDL file.\n")
        self.header_file.write(f" *\n")
        self.header_file.write(f" * This file was generated by the tool gen.\n")
        self.header_file.write(f" */\n")
        self.header_file.write(f"\n")
        self.header_file.write(f"#ifndef _{self.module_name}_H_\n")
        self.header_file.write(f"#define _{self.module_name}_H_\n")
        self.header_file.write(f"\n")
        self.header_file.write(f"#ifdef __cplusplus\n")
        self.header_file.write(f"extern \"C\"\n")
        self.header_file.write(f"{{\n")
        self.header_file.write(f"#endif\n")
        self.header_file.write(f"\n")
        self.header_file.write(f"#include <stdint.h>\n")
        self.header_file.write(f"#include <stdbool.h>\n")
        for include in self.includes:
            self.header_file.write(f"#include \"{include}\"\n")
        self.header_file.write(f"\n")

    def init_source(self):
        self.source_file.write(f"/*!\n")
        self.source_file.write(f" * @file {self.basename}.c\n")
        self.source_file.write(f" * This source file contains the definition of the described types in the IDL file.\n")
        self.source_file.write(f" *\n")
        self.source_file.write(f" * This file was generated by the tool gen.\n")
        self.source_file.write(f" */\n")
        self.source_file.write(f"\n")
        self.source_file.write(f"#include \"{self.basename}.h\"\n")
        self.source_file.write(f"\n")
        self.source_file.write(f"#include \"ucdr/microcdr.h\"\n")
        self.source_file.write(f"#include <string.h>\n")

    def end_header(self):
        self.header_file.write(f"\n")
        self.header_file.write(f"#ifdef __cplusplus\n")
        self.header_file.write(f"}}\n")
        self.header_file.write(f"#endif\n")
        self.header_file.write(f"\n")
        self.header_file.write(f"#endif // _{self.module_name}_H_")

    def end_source(self):
        pass


def to_snake_case(text: str):
    """
    @ref https://stackoverflow.com/a/12867228
    :param text: A string in CamelCase
    :return: return text in snake_case
    """
    regex = re.compile('((?<=[a-z0-9])[A-Z]|(?!^)[A-Z](?=[a-z]))')
    return regex.sub(r'_\1', text)
