import difflib
import os
import unittest

from parameterized import parameterized
from os import listdir
from os.path import isfile, join

from NarniaDDSGen.parser import Parser
from NarniaDDSGen.scanner import Scanner
from NarniaDDSGen.visitor import ASTVisitor

target_idls_path = os.path.join('.', 'target_idls')
target_idls_out_path = os.path.join('.', 'target_idls_out')
target_idls_temp = os.path.join('.', 'target_temp')

class TestSharkOSIDLs(unittest.TestCase):

    def setUp(self):
        self.maxDiff = None
        if not os.path.isdir(target_idls_temp):
            os.mkdir(target_idls_temp)

    @parameterized.expand(os.path.join(target_idls_path, f) for f in listdir(target_idls_path) if isfile(join(target_idls_path, f)))
    def test_sequence(self, name):
        scanner = Scanner(name)
        tokens = scanner.scan_tokens()
        self.assertFalse(scanner.had_error)

        parser = Parser(name, tokens)
        ast = parser.parse()

        self.assertFalse(parser.had_error)

        visitor = ASTVisitor(ast, name, target_idls_temp)
        visitor.write_ast()

        self.assertFalse(scanner.had_error)

        generated_header_path = visitor.idl_filename_to_header(name, target_idls_temp)
        generated_source_path = visitor.idl_filename_to_source(name, target_idls_temp)
        target_header_path = visitor.idl_filename_to_header(name, target_idls_out_path)
        target_source_path = visitor.idl_filename_to_source(name, target_idls_out_path)

        with open(generated_header_path) as ff:
            generated_header_lines = ff.readlines()
        with open(target_header_path) as tf:
            target_header_lines = tf.readlines()
        with open(generated_source_path) as ff:
            generated_source_lines = ff.readlines()
        with open(target_source_path) as tf:
            target_source_lines = tf.readlines()

        diff_header = list(difflib.context_diff(generated_header_lines, target_header_lines, generated_header_path, target_header_path))
        diff_source = list(difflib.context_diff(generated_source_lines, target_source_lines, generated_source_path, target_source_path))

        self.assertFalse(bool(diff_header), ''.join(diff_header))
        self.assertFalse(bool(diff_source), ''.join(diff_source))


def main():  # pragma: no cover
    unittest.main()
