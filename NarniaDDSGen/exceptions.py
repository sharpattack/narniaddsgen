import os


class ParseError(Exception):
    pass


def report(file: os.path, line: int, where: str = "", message: str = ""):
    print(f"[{file}:{line}] Error {where}{f': {message}' if message else ''}")
