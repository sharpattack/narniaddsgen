import os

from NarniaDDSGen.exceptions import report
from NarniaDDSGen.preprocessor import Preprocessor
from NarniaDDSGen.Token import Token, TokenType
from NarniaDDSGen.token_type import TokenMap


class Scanner:
    def __init__(self, source_path: os.path):
        self.input_files = None
        self.input_files_data = None
        self.had_error = False
        self.start = 0
        self.current = 0
        self.line = 1
        self.current_file = ""
        self.tokens = []

        preprocessor = Preprocessor(source_path)

        self.input_files = os.path.abspath(source_path)
        self.input_files_data = preprocessor.get_preprocessed_file()

    def scan_tokens(self):
        self.current_file = self.input_files

        while not self.is_at_end():
            self.start = self.current
            self.scan_token()

        self.tokens.append(Token(TokenType.EOF, "", None, self.line))

        return self.tokens

    def scan_token(self):
        char = self.advance()
        match char:
            case '(': (
                self.add_token(TokenType.LEFT_PAREN))
            case ')': (
                self.add_token(TokenType.RIGHT_PAREN))
            case '{': (
                self.add_token(TokenType.LEFT_BRACE))
            case '}': (
                self.add_token(TokenType.RIGHT_BRACE))
            case '[': (
                self.add_token(TokenType.LEFT_SQUARE_BRACKET))
            case ']': (
                self.add_token(TokenType.RIGHT_SQUARE_BRACKET))
            case '<': (
                self.add_token(TokenType.LEFT_ANGLE_BRACKET))
            case '>': (
                self.add_token(TokenType.RIGHT_ANGLE_BRACKET))
            case ',': (
                self.add_token(TokenType.COMMA))
            case '.': (
                self.add_token(TokenType.DOT))
            case '-': (
                self.add_token(TokenType.MINUS))
            case ':': (
                self.add_token(TokenType.COLON))
            case '+': (
                self.add_token(TokenType.PLUS))
            case ';': (
                self.add_token(TokenType.SEMICOLON))
            case '/': (
                self.add_token(TokenType.SLASH))
            case '*': (
                self.add_token(TokenType.STAR))
            case '=': (
                self.add_token(TokenType.EQUAL))
            case '#': (
                self.add_token(TokenType.HASHTAG))
            case '"': (
                self.string())
            case ' ' | '\t' | '\r':
                pass
            case '\n':
                self.line += 1
            case _:
                if char.isdigit():
                    self.number()
                elif self.is_alpha(char):
                    self.identifier()
                else:
                    self.error(self.current_file, self.line, "Unexpected character.", f'"{char}"')

    def peek(self) -> str:
        if self.is_at_end():
            return '\0'
        return self.input_files_data[self.current]

    def peek_next(self) -> str:
        if self.current + 1 >= len(self.input_files_data):
            return '\0'
        return self.input_files_data[self.current + 1]

    def advance(self) -> str:
        char = self.input_files_data[self.current]
        self.current += 1
        return char

    def string(self):
        while self.peek() != '"' and not self.is_at_end():
            if self.peek() == '\n':
                self.line += 1
            self.advance()

        if self.is_at_end():
            self.report_error(self.current_file, self.line, "Unterminated string.")
            return

        # The closing ".
        self.advance()

        value = ''.join(self.input_files_data[self.start + 1:self.current - 1])
        self.add_token(TokenType.STRING, value)

    def number(self):
        is_float = False

        while self.peek().isdigit():
            self.advance()

        if self.peek() == '.' and self.peek_next().isdigit():
            is_float = True
            self.advance()
            while self.peek().isdigit():
                self.advance()

        value = ''.join(self.input_files_data[self.start:self.current])

        self.add_token(TokenType.NUMBER, float(value) if is_float else int(value))

    def identifier(self):
        while self.is_alphanum(self.peek()):
            self.advance()

        value = ''.join(self.input_files_data[self.start:self.current])
        type = TokenType.IDENTIFIER
        try:
            type = TokenMap[value]
        except KeyError:
            pass
        self.add_token(type)

    def match(self, expected: str) -> bool:
        if self.is_at_end():
            return False
        elif self.input_files_data[self.current] != expected:
            return False
        self.current += 1
        return True

    def add_token(self, type: TokenType, literal=None):
        text = self.input_files_data[self.start:self.current]
        self.tokens.append(Token(type, ''.join(text), literal, self.line))


    def is_at_end(self):
        return self.current >= len(self.input_files_data)

    def is_alpha(self, char: str) -> bool:
        return char.isalpha() or char == '_'

    def is_alphanum(self, char: str) -> bool:
        return self.is_alpha(char) or char.isdigit()

    def error(self, file: os.path, line: int, where: str = "", message: str = ""):
        self.had_error = True
        report(file, line, where, message)
