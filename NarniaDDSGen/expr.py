# Generated on 2024-07-24 15:46:01.561286
# DO NOT MODIFY

from NarniaDDSGen.Token import Token


class Expr:
    def visit(self, visitor):
        return ''


class Binary(Expr):
    def __init__(self, left: Expr, operator: Token, right: Expr):
        self.left = left
        self.operator = operator
        self.right = right

    def visit(self, visitor):
        return visitor.visit(self)

    def __str__(self):
        return f"{self.left} {self.operator} {self.right}"

    def __repr__(self):
        return str(self)


class Grouping(Expr):
    def __init__(self, expression: Expr):
        self.expression = expression

    def visit(self, visitor):
        return visitor.visit(self)

    def __str__(self):
        return f"{self.expression} "

    def __repr__(self):
        return str(self)


class Literal(Expr):
    def __init__(self, value: object):
        self.value = value

    def visit(self, visitor):
        return visitor.visit(self)

    def __str__(self):
        return f"{self.value} "

    def __repr__(self):
        return str(self)


class Unary(Expr):
    def __init__(self, operator: Token, right: Expr):
        self.operator = operator
        self.right = right

    def visit(self, visitor):
        return visitor.visit(self)

    def __str__(self):
        return f"{self.operator} {self.right} "

    def __repr__(self):
        return str(self)


class Variable(Expr):
    def __init__(self, name: Token):
        self.name = name

    def visit(self, visitor):
        return visitor.visit(self)

    def __str__(self):
        return f"{self.name} "

    def __repr__(self):
        return str(self)

