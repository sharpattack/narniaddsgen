/*!
 * @file debug.c
 * This source file contains the definition of the described types in the IDL file.
 *
 * This file was generated by the tool gen.
 */

#include "debug.h"

#include "ucdr/microcdr.h"
#include <string.h>

bool Debug_serialize_topic(ucdrBuffer* writer, const Debug* topic)
{
    bool success = true;
        success &= ucdr_serialize_string(writer, topic->message);
    return success && !writer->error;
}

bool Debug_deserialize_topic(ucdrBuffer* reader, Debug* topic)
{
    bool success = true;
        success &= ucdr_deserialize_string(reader, topic->message, 255);
    return success && !reader->error;
}

uint32_t Debug_size_of_topic(const Debug* topic, uint32_t size)
{
    uint32_t previousSize = size;
        size += ucdr_alignment(size, 4) + 4 + (uint32_t)strlen(topic->message) + 1;
    return size - previousSize;
}
