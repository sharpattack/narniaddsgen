
# Introduction

NarniaDDSGen est une réimplementation [Micro-XRCE-DDS-Gen](https://github.com/eProsima/Micro-XRCE-DDS-Gen) en python.

Le but est de se débarassé de la dépendance à Java du transpileur original mais aussi de gagné en liberté (ajout de features, correction de bug, etc.)

Ce transpiler a été construit grace au livre [Crafting Interpreter](https://craftinginterpreters.com/)

## Tutorial

Le langage IDL permet de décrire des interfaces communes dans le but d'être transpilé dans un ou plusieurs langages cible.
L'idée étant que peu importe le langage final, les sérialization et désérialization sont toujours identique

Un fichier IDL est composé d'une ou plusieurs structures, potentiellement des constantes et éventuellement des directives
de préprocesseur.

### Commentaire

Le langage IDL supporte 2 types de commentaire :

1. `//` : Commentaire sur une seule ligne. Tout ce qui est après ce commentaire est ignoré jusqu'à la fin de la ligne.
2. `/* [...] */` : Commentaire de bloc. Tout ce qui est entre ces deux symboles est ignoré.

### Structure

Une structure se déclare de la manière suivante :
```idlang
struct <nom_du_type>
{
    [...]
};
```
Une structure peu contenir autant d'attribut que nécessaire.

Un type se déclare de la manière suivante :
```idlang
<type> <nom_de_lattribut>[<taille>];
```

`<type>` peu soit être un type simple, auquel cas il peu prendre une des valeur suivante :

| IDL                  | C           |
|----------------------|-------------|
| `float`              | `float`     |
| `double`             | `double`    |
| `short`              | `int16_t`   |
| `long`               | `int32_t`   |
| `long long`          | `int64_t`   |
| `unsigned short`     | `uint16_t`  |
| `unsigned long`      | `uint32_t`  |
| `unsigned long long` | `uint64_t`  |
| `char`               | `char`      |
| `boolean`            | `bool`      |
| `octet`              | `uint8_t`   |
| `string`             | `char[255]` |
| `string<N>`          | `char[N]`   |
| `sequence<type, N>`  | `type[N]`   |

`sequence<type, N>` est un cas particulier, car c'est un tableau dynamique.
Déclarer un tableau dynamique créé automatiquement une variable `<nom_de_lattribut>_size` de type `uint32_t`.

Dans le cas où on chaine plusieurs tableaux dynamiques, par exemple `sequence<sequence<long, 10>, 20>`, une variable
de taille est créé par index du tableau dynamique correspondant. Dans l'exemple, on a donc `<nom_de_lattribut>_size0` et `<nom_de_lattribut>_size1`.

Dans le cas où ont créé un tableau de tableau dynamique, la variable de taille devient un tableau de la taille du tableau parent.

Pour créer un tableau static, il faut ajouter une taille à la fin du nom de la variable. Par exemple `long var[10];`
correspond à un tableau de 10 `int32_t`.

Il est possible de déclaré un tableau à plusieur dimension en utilisant plusieurs spécifieur de taille.
Par exemple `long var[10][20]` créer un tableau de 10 tableau de 20 `int32_t`.

Il est possible d'utiliser une structure précédement déclaré comme type de variable dans une autre structure.

#### Exemples

Une structure simple :
```idlang
struct Hello
{
    string message;
    unsigned long index;
};
```

Une structure utilisant une autre structure :
```idlang
struct Gizmo
{
    float x;
    float y;
    float z;
};

struct Object
{
    Gizmo position;
    Gizmo angle;
};
```

Une structure contenant un tableau dynamique de string
```idlang
struct Debug
{
    sequence<string<128>, 25> message;
};
```

### Constante

Les constantes sont traduite en `#define` au transpilage.

Pour déclarer une constante, on utilise la même syntaxe que la déclaration des attributs d'une structure.
La seule différence étant la présence du mot clef `const` avant la déclaration ainsi que l'assignation d'une valeur.

La déclarion ce fait donc de cette manière :

```idlang
const <type> <nom_de_la_constante>[<taille>] = <valeur>;
```

:warning: Actuellement, seul les constante de nombre entier en base 10 sont supporté !!! :warning:

Les constantes peuvent être utiliser dans les tailles des attributs.

#### Exemples

Une constante
```idlang
const unsigned long MATCH_LENGTH = 120;
```

Plusieurs constantes utilisées comme taille de tableau
```idlang

const unsigned long SCREEN_WIDTH = 640;
const unsigned long SCREEN_HEIGHT = 480;

struct Screen
{
    octet pixels[SCREEN_WIDTH][SCREEN_HEIGHT];
    sequence<unsigned long, SCREEN_WIDTH * SCREEN_HEIGHT> pixel_to_redraw;
}
```

### Preprocessor

Le langage IDL supporte la commande de préprocesseur `#include "<path>"`, ou `<path>` correspond à un chemin vers un IDL.

Le chemin lui même n'est pas résolu. Dans le cadre de ce transpileur, le chemin est traduit en `#include` d'un header comme
si le chemin du fichier `.idl` était valide.

#### Exemples

Un fichier définissant un type est un autre fichier l'utilisant :

```idlang
// Fichier A - message.idl

struct Message
{
    string contenu;
    unsigned short type;
}
```

```idlang
// Fichier B - messagerie.idl

#include "message.idl"

struct Messagerie
{
    sequence<Message, 100> en_attente;
}
```

### Arithmétique

Le langage IDL supporte une utilisation mineure de l'arithmétique pour définir les tailles de tableau.

Les opérateurs suivants sont supporté : `+-*/()`

:warning: Aucun calcule n'est fait par le transpileur. Le compilateur final (e.g. `gcc`) s'en chargera :warning:

#### Exemples

```idlang
struct Processor
{
    octet ram[1024*128];
    octet rom[1024*512];
}
```

## Détails d'implémentations
- Les variables constantes sont généré dans les fichiers de headers sous forme de `#define`
- Toutes les constantes entières sont considéré au minimum comme des int 32bits (suffix `L`)

## Futures améliorations
- Support des enums
- Support du prefix hexadecimal
- Support du `wchar_t`
- Support du `long double`
- Support de l'héritage
- Réparer les constantes