# Generated on 2024-07-24 15:46:01.561382
# DO NOT MODIFY

from NarniaDDSGen.Token import Token
from NarniaDDSGen.expr import Expr


class Stmt:
    def visit(self, visitor):
        return ''


class Var(Stmt):
    def __init__(self, type: list[Token], name: Token, size: list[Expr]):
        self.type = type
        self.name = name
        self.size = size

    def visit(self, visitor):
        return visitor.visit(self)

    def __str__(self):
        return f"{self.type} {self.name} {self.size}"

    def __repr__(self):
        return str(self)


class ConstVar(Stmt):
    def __init__(self, type: list[Token], name: Token, value: Expr):
        self.type = type
        self.name = name
        self.value = value

    def visit(self, visitor):
        return visitor.visit(self)

    def __str__(self):
        return f"{self.type} {self.name} {self.value}"

    def __repr__(self):
        return str(self)


class Struct(Stmt):
    def __init__(self, name: Token, superStruct: Expr, statements: list[Var]):
        self.name = name
        self.superStruct = superStruct
        self.statements = statements

    def visit(self, visitor):
        return visitor.visit(self)

    def __str__(self):
        return f"{self.name} {self.superStruct} {self.statements}"

    def __repr__(self):
        return str(self)


class Expression(Stmt):
    def __init__(self, expr: Expr):
        self.expr = expr

    def visit(self, visitor):
        return visitor.visit(self)

    def __str__(self):
        return f"{self.expr} "

    def __repr__(self):
        return str(self)


class Include(Stmt):
    def __init__(self, path: str):
        self.path = path

    def visit(self, visitor):
        return visitor.visit(self)

    def __str__(self):
        return f"{self.path} "

    def __repr__(self):
        return str(self)

