import os
from typing import Union

from NarniaDDSGen.Token import Token
from NarniaDDSGen.exceptions import report, ParseError
from NarniaDDSGen.expr import *
from NarniaDDSGen.stmt import *
from NarniaDDSGen.token_type import TokenType


class Parser:
    def __init__(self, file: os.path, tokens: list[Token]):
        self.tokens = tokens
        self.current_file = file
        self.current = 0
        self.had_error = False

    def parse(self) -> list[Stmt]:
        statements = []
        while not self.is_at_end():
            decl = self.declaration()
            if decl:
                statements.append(decl)
        return statements

    def expression(self):
        return self.assignement()

    def declaration(self):
        try:
            if self.match(TokenType.HASHTAG):
                return self.preprocessorDeclaration()

            if self.match(TokenType.STRUCT):
                return self.structDeclaration()

            if self.match(TokenType.CONST):
                return self.constVarDeclaration()

            raise self.error(self.peek(), "Expected struct or const variable")
        except ParseError:
            self.synchronize()
            return None

    def preprocessorDeclaration(self):
        self.consume(TokenType.INCLUDE, "Expect keyword include.")
        path = self.consume(TokenType.STRING, "Expect a path.")
        return Include(path.lexeme)

    def structDeclaration(self):
        name = self.consume(TokenType.IDENTIFIER, "Expect struct name.")

        superClass = None
        if self.match(TokenType.COLON):
            self.consume(TokenType.IDENTIFIER, "Expect superstruct name.")
            superClass = Variable(self.previous())

        self.consume(TokenType.LEFT_BRACE, "Expect '{' before struct body.")

        attributes = []
        while (not self.check(TokenType.RIGHT_BRACE) and not self.is_at_end()):
            attributes.append(self.variableDeclaration())

        self.consume(TokenType.RIGHT_BRACE, "Expect '}' before struct body.")
        self.consume(TokenType.SEMICOLON, "Expect ';' at end of struct body.")

        return Struct(name, superClass, attributes)

    def constVarDeclaration(self):
        type_list = self.type()
        name = self.consume(TokenType.IDENTIFIER, "Expect const variable name.")
        self.consume(TokenType.EQUAL, "Expect '='.")
        value = self.term()
        self.consume(TokenType.SEMICOLON, "Expect ';' at end of const var declaration.")
        return ConstVar(type_list, name, value)

    def variableDeclaration(self):
        type_list = self.type()
        name = self.consume(TokenType.IDENTIFIER, "Expect variable name.")
        size = []
        while self.match(TokenType.LEFT_SQUARE_BRACKET):
            size.append(self.term())
            self.consume(TokenType.RIGHT_SQUARE_BRACKET, "Expect ']'.")

        self.consume(TokenType.SEMICOLON, "Expect ';' at end of var declaration.")

        return Var(type_list, name, size)

    def type(self) -> list[Union[Expr, Token]]:
        type_list = []

        self.recursive_type(type_list)

        return type_list

    def recursive_type(self, type_list: list[Union[Expr, Token]]):
        if self.match(TokenType.BOOLEAN, TokenType.CHAR, TokenType.OCTET, TokenType.FLOAT, TokenType.DOUBLE, TokenType.IDENTIFIER):
            type_list.append(self.previous())
            return

        has_unsigned = False
        if self.match(TokenType.UNSIGNED):
            has_unsigned = True
            type_list.append(self.previous())

        if self.match(TokenType.SHORT, TokenType.LONG):
            type_list.append(self.previous())
            if self.previous().type == TokenType.LONG and self.check(TokenType.LONG):
                type_list.append(self.advance())
            return
        elif has_unsigned:
            raise self.error(self.previous(), "Expect type.")

        if self.match(TokenType.STRING):
            type_list.append(self.previous())
            if self.match(TokenType.LEFT_ANGLE_BRACKET):
                type_list.append(self.term())
                self.consume(TokenType.RIGHT_ANGLE_BRACKET, "Expect '>'.")
            else:
                type_list.append(Literal(255))
            return

        if self.match(TokenType.SEQUENCE):
            type_list.append(self.previous())
            self.consume(TokenType.LEFT_ANGLE_BRACKET, "Expect '<' after sequence.")
            self.recursive_type(type_list)

            if self.match(TokenType.COMMA):
                type_list.append(self.term())
            else:
                type_list.append(Literal(100))

            self.consume(TokenType.RIGHT_ANGLE_BRACKET, "Expect '>' at end of sequence.")
            return

    def assignement(self):
        return self.term()

    def term(self) -> Expr:
        expr = self.factor()

        while self.match(TokenType.MINUS, TokenType.PLUS):
            operator = self.previous()
            right = self.factor()
            expr = Binary(expr, operator, right)

        return expr

    def factor(self) -> Expr:
        expr = self.unary()

        while self.match(TokenType.SLASH, TokenType.STAR):
            operator = self.previous()
            right = self.unary()
            expr = Binary(expr, operator, right)

        return expr

    def unary(self) -> Expr:
        if self.match(TokenType.MINUS):
            operator = self.previous()
            right = self.unary()
            return Unary(operator, right)
        return self.primary()


    def primary(self) -> Expr:
        if self.match(TokenType.NUMBER, TokenType.STRING):
            return Literal(self.previous().literal)

        if self.match(TokenType.IDENTIFIER):
            return Variable(self.previous())

        if self.match(TokenType.LEFT_PAREN):
            expr = self.expression()
            self.consume(TokenType.RIGHT_PAREN, "Expect ')' after expression.")
            return Grouping(expr)

        raise self.error(self.peek(), "Expect expression.")


    def synchronize(self):
        self.advance()
        while not self.is_at_end():
            if self.previous().type == TokenType.SEMICOLON:
                return

            match self.peek().type:
                case TokenType.STRUCT | TokenType.UNSIGNED | TokenType.CHAR | TokenType.SHORT | TokenType.LONG | TokenType.OCTET | TokenType.FLOAT | TokenType.DOUBLE:
                    return

            self.advance()

    def match(self, *token_types:TokenType):
        for token_type in token_types:
            if self.check(token_type):
                self.advance()
                return True
        return False

    def check(self, token_type: TokenType) -> bool:
        if self.is_at_end():
            return False
        return self.peek().type == token_type

    def advance(self) -> Token:
        if not self.is_at_end():
            self.current += 1
        return self.previous()

    def peek(self) -> Token:
        return self.tokens[self.current]

    def previous(self) -> Token:
        return self.tokens[self.current - 1]

    def is_at_end(self) -> bool:
        return self.peek().type == TokenType.EOF

    def consume(self, token_type:TokenType, message:str) -> Token:
        if self.check(token_type):
            return self.advance()
        raise self.error(self.peek(), message)

    def error(self, token: Token, message: str):
        self.had_error = True
        self.report(token, message)
        return ParseError()

    def report(self, token: Token, message: str):
        if token.type == TokenType.EOF:
            report(self.current_file, token.line, "at end", message)
        else:
            report(self.current_file, token.line, f"at '{token.lexeme}'", message)




