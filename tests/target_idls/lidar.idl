struct LidarVector
{
  // a vector, in m
  float x;
  float y;
};

struct LidarPolygon
{
  LidarVector points[10];
  // number of pointsin the polygon
  unsigned short pointsNb;
};


struct LidarPoseEstim
{
  // if true, measure is considered correct. Otherwise, the measure
  // should be ignored.
  boolean isValid;
  // timestamp, in ms
  long long timestamp;
  // estimated pos vector in m, in the global frame
  LidarVector pos;
  // estimated angle in radians, in the global frame
  float angle;
};


// LidarInfos has a generic name (because we were not sure of all the
// information to transmit), but it actually correspond to
// *additional* information for debugging or visualization purposes
struct LidarInfo
{
  // bounding boxes of the tracked objects, in m, in the lidar frame
  // for now, this only corresponds to beacons
  LidarPolygon trackedObjects[20];
  // number of tracked objects sent
  unsigned short trackedObjectsNb;

  // current point cloud, in m, in the lidar frame
  LidarVector pointCloud[1000];
  // number of points in the cloud
  unsigned short pointsNb;
};


// Information about enemy robots
struct LidarEnemyInfo
{
    // bounding boxes of the detected enemy robots
    LidarPolygon enemyBoxes[10];
    // number of enemy robots
    unsigned short enemiesNb;
};


struct LidarProximity
{
  // True if at least an object is considered close enough to warrant
  // detection
  boolean anObjectIsClose;
  // distance in m
  float distance;

  // An array of min distance divided by angle zones. Zones are 1/8th
  // of a full angle, starting from 0 rad and increasing, as in the
  // following diagram:
  //
  //               y
  //               ^
  //            \  |  /
  //            3\2|1/0
  //           --- o ---> x
  //            4/5|6\7
  //            /  |  \
  // 
  // The x and y axis are in the *local* coordinates of the robot
  float proximity[8];

};
