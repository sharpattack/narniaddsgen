"""Entry point for project_name."""
import sys
from NarniaDDSGen.cli import main  # pragma: no cover

if __name__ == "__main__":  # pragma: no cover
    sys.exit(main())