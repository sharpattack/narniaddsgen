from enum import Enum, auto
from typing import Final


class TokenType(Enum):
    # Single-character tokens.
    LEFT_PAREN = auto()
    RIGHT_PAREN = auto()
    LEFT_BRACE = auto()
    RIGHT_BRACE = auto()
    LEFT_ANGLE_BRACKET = auto()
    RIGHT_ANGLE_BRACKET = auto()
    LEFT_SQUARE_BRACKET = auto()
    RIGHT_SQUARE_BRACKET = auto()
    COMMA = auto()
    DOT = auto()
    COLON = auto()
    MINUS = auto()
    PLUS = auto()
    SEMICOLON = auto()
    SLASH = auto()
    STAR = auto()
    EQUAL = auto()
    HASHTAG = auto()

    # Literals.
    IDENTIFIER = auto()
    NUMBER = auto()

    # Keywords.
    STRUCT = auto()
    CONST = auto()
    INCLUDE = auto()

    # Types.
    UNSIGNED = auto()
    BOOLEAN = auto()
    CHAR = auto()
    OCTET = auto()
    SHORT = auto()
    LONG = auto()
    FLOAT = auto()
    DOUBLE = auto()
    STRING = auto()
    SEQUENCE = auto()

    EOF = auto()


TokenMap: Final[dict] = {
    "struct": TokenType.STRUCT,
    "const": TokenType.CONST,
    "unsigned": TokenType.UNSIGNED,
    "boolean": TokenType.BOOLEAN,
    "char": TokenType.CHAR,
    "octet": TokenType.OCTET,
    "short": TokenType.SHORT,
    "long": TokenType.LONG,
    "float": TokenType.FLOAT,
    "double": TokenType.DOUBLE,
    "string": TokenType.STRING,
    "sequence": TokenType.SEQUENCE,
    "include": TokenType.INCLUDE,
}
