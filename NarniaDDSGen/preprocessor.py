import os
import re


class Preprocessor:
    def __init__(self, file: os.path):
        self.current_file = file
        with open(file, 'r') as in_file:
            self.file_data = list(in_file.read().replace(r"\r\n", r"\n"))

        self.strip_comments()

    def get_preprocessed_file(self) -> list:
        return self.file_data

    def strip_comments(self):
        index = 1
        while index < len(self.file_data):
            char1 = self.file_data[index - 1]
            char2 = self.file_data[index]

            if char1 == '/' and char2 == '/':
                index -= 1
                while index < len(self.file_data) and self.file_data[index] != '\n':
                    del self.file_data[index]
            elif char1 == '/' and char2 == '*':
                index -= 1
                while index < len(self.file_data) and self.file_data[index - 1] != '*' and self.file_data[index - 1] != '/':
                    del self.file_data[index]

            index += 1

    def filename_to_absolute_path(self, filename) -> str:
        current_file_directory = os.path.dirname(self.current_file)
        file_path = os.path.join(current_file_directory, filename)
        absolute_path = os.path.abspath(file_path)
        return absolute_path

