"""Entry point for project_name."""

from tests.narniatest import main  # pragma: no cover

if __name__ == "__main__":  # pragma: no cover
    main()